<!doctype >
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Script API-153</title>
</head>
<body>
<?php

/*abrimos archivo de texto para nombres */

$con = mysqli_connect('localhost', 'root', 'extendeal', 'extendeal_ar');  

function escaparCaracteres($con, $string){
  return mysqli_real_escape_string($con, trim($string));
}

/* cargamos los arrays con info random */

$txtNombres = fopen("nombres.txt", "r") or die("Unable to open file!");  /* Abrimos el archivo de nombres */
$txtDirecciones = fopen("direcciones.txt", "r") or die("Unable to open file!");
$txtLocalidades = fopen("localidades.txt", "r") or die("Unable to open file!");
$txtCPs = fopen("codigos postales.txt", "r") or die("Unable to open file!");
$txtCelulares = fopen("telefonos.txt", "r") or die("Unable to open file!");
$txtEmails = fopen("emails.txt", "r") or die("Unable to open file!");
$txtRazonSoc = fopen("razones sociales.txt", "r") or die("Unable to open file!");

for($i = 0; $i < 10; $i++) {

  if($strNombre = fgets($txtNombres))
    $cliNames[$i] = escaparCaracteres($con, $strNombre);
  else
    $cliNames[$i] = "Sarasa";

  if($strDireccion = fgets($txtDirecciones))
    $cliAddresses[$i] = escaparCaracteres($con, $strDireccion);
  else
    $cliAddresses[$i] = "Calle Falsa 123";

  if($strLatitud = fgets($txtDirecciones))
    $dirLat[$i] = escaparCaracteres($con, $strLatitud);
  else
    $dirLat[$i] = "0.0000000000";

  if($strLongitud = fgets($txtDirecciones))
    $dirLong[$i] = escaparCaracteres($con, $strLongitud);
  else
    $dirLong[$i] = "0.0000000000";

  if($strLocalidad = fgets($txtLocalidades))
    $cliLocalities[$i] = escaparCaracteres($con, $strLocalidad);
  else
    $cliLocalities[$i] = "Calle Falsa 123";

  if($strCP = fgets($txtCPs))
    $cliCPs[$i] = escaparCaracteres($con, $strCP);
  else
    $cliCPs[$i] = "0123";

  if($strCod = fgets($txtCelulares))
    $celCod[$i] = escaparCaracteres($con, $strCod);
  else
    $celCod[$i] = "00";

  if($strCel = fgets($txtCelulares))
    $telCel[$i] = escaparCaracteres($con, $strCel);
  else
    $telCel[$i] = "00000000";

  if($strMail = fgets($txtEmails))
    $cliMails[$i] = escaparCaracteres($con, $strMail);
  else
    $cliMails[$i] = "asd@asd.com";

  if($strRazon = fgets($txtRazonSoc))
    $cliRazones[$i] = escaparCaracteres($con, $strRazon);
  else
    $cliRazones[$i] = "Avenida Siempreviva 123";
}

fclose($txtNombres);
fclose($txtDirecciones);
fclose($txtLocalidades);
fclose($txtCPs);
fclose($txtCelulares);
fclose($txtEmails);
fclose($txtRazonSoc);

/* 
tablas a afectar:
clients
users
partners
delivery_points
*/

$myFile = fopen("Script API-153.sql", "w") or die("Unable to open file!");

/* clients */
/******************/

$resultado = $con->query("SELECT * FROM clients");
while(($row = $resultado->fetch_assoc()) != FALSE) {

  $txt = 'UPDATE clients SET name = "' . $cliNames[rand(0,9)] . 
  '", legal_address = "' . $cliAddresses[rand(0,9)] .  
  '", phone = "' . $celCod[$iTel = rand(0,9)] . $telCel[$iTel] .
  '", legal_name = "' . $cliRazones[rand(0,9)] .  
  '" WHERE id = ' . $row['id'] . 
  ';
  ';
  fwrite($myFile, $txt);
}

/* usuarios */

$resultado = $con->query("SELECT * FROM users");
while(($row = $resultado->fetch_assoc()) != FALSE) {
  $txt = 'UPDATE users SET name = "' . $cliNames[rand(0,9)] . 
  '", email = "' . 'dev+user' . $row['id'] . '@extendeal.com' .
  '", phone = "' . $celCod[$iTel = rand(0,9)] . $telCel[$iTel] .    
  '" WHERE id = ' . $row['id'] . 
  ';
  ';
  fwrite($myFile, $txt);
}

/* partners */

$resultado = $con->query("SELECT * FROM partners");
while(($row = $resultado->fetch_assoc()) != FALSE) {
  $txt = 'UPDATE partners SET name = "' . $cliNames[rand(0,9)] . 
  '", email = "' . 'dev+asociado' . $row['id'] . '@extendeal.com' .
  '", alternative_email = "' . $cliMails[rand(0,9)] .    
  '", code_phone = "' . $celCod[$iTel = rand(0,9)] .    
  '", cellphone_number = "' . $telCel[$iTel] .   
  '", address = "' . $cliAddresses[$iDir = rand(0,9)] .    
  '", longitude = "' . $dirLat[$iDir] .  // Buscar lat y long en adress. clavar lat y long en el mismo array que adress.
  '", latitude = "' . $dirLong[$iDir] .   //
  '" WHERE id = ' . $row['id'] . 
  ';
  ';
  fwrite($myFile, $txt);
}

/* delivery_points */

$resultado = $con->query("SELECT * FROM delivery_points");
while(($row = $resultado->fetch_assoc()) != FALSE) {
  $txt = 'UPDATE delivery_points SET address = "' . $cliAddresses[$iDir = rand(0,9)] . 
  '", zip_code = "' . $cliCPs[rand(0,9)] .    
  '", contact_name = "' . $cliNames[rand(0,9)] .    
  '", phone = "' . $celCod[$iTel = rand(0,9)] . $telCel[$iTel] .  
  '", email = "' . 'nombre.apellido+puntodeentrega@extendeal.com' .    
  '", longitude = "' . $dirLat[$iDir] .  // Buscar lat y long en adress. clavar lat y long en el mismo array que adress.
  '", latitude = "' . $dirLong[$iDir] .   //
  '" WHERE id = ' . $row['id'] . 
  ';  
  ';
  fwrite($myFile, $txt);
}

echo 'The script has been created.';  

fclose($myFile); 

?>
</body>
</html>

