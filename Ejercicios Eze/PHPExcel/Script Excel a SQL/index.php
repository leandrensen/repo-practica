<!doctype >
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Script Generator</title>
</head>
<body>
<?php
  $con = mysqli_connect('localhost', 'root', '', 'extendeal_ar');
  

  function escaparCaracteres($con, $string){
    return mysqli_real_escape_string($con, trim($string));
  }

  require_once "Classes/PHPExcel.php";

  $tmpFnName = 'Direcciones_Clientes_Extendeal.xlsx';
  $excelReader = PHPExcel_IOFactory::createReaderForFile($tmpFnName);
  $excelObj = $excelReader->load($tmpFnName);
  $workSheet = $excelObj->getActiveSheet();
  $lastRow = $workSheet->getHighestRow();
 
  $myfile = fopen("SQLScript.sql", "w") or die("Unable to open file!"); 

  $txt = 'ALTER TABLE delivery_points ADD notas VARCHAR(255);
  
  ';
  fwrite($myfile, $txt);

  for($row = 2; $row <= $lastRow; $row++) {
    $txt = 'UPDATE delivery_points SET name = "' . escaparCaracteres($con, $workSheet->getCell('B'.$row)->getValue()) . 
    '", address = "' . escaparCaracteres($con, $workSheet->getCell('C'.$row)->getValue()) .  
    '", notas = "' . escaparCaracteres($con, $workSheet->getCell('G'.$row)->getValue()) .
    '" WHERE id = ' . $workSheet->getCell('H'.$row)->getValue() . 
    ';
    ';
    fwrite($myfile, $txt);
  }

  echo 'The script has been created.';  

  fclose($myfile); 
?>
</body>
</html>
