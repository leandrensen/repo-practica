<?php
require_once 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet as Worksheet;

/**
 * Description: Fetchs all the values (column by column) in the specified row
 * @param $worksheet (excel worksheet)
 * @param $row (numeric value)
 * @return array
 * @throws \PhpOffice\PhpSpreadsheet\Exception
 */
function getRowValues(Worksheet $worksheet, $row) {
  $rowValues = [];

  // get the number of the las column
  $lastColumn = $worksheet->getHighestColumn();
  $lastColumnIndex = Coordinate::columnIndexFromString($lastColumn);
  $i = 0;

  for($col = 1; $col <= $lastColumnIndex; $col++, $i++) {
    $cell = $worksheet->getCellByColumnAndRow($col, $row);
    $rowValues[$i] = $cell->getValue();
  }

  return $rowValues;
}

/**
 * Description: Fills the unalignedField with a predefined char until it fits the size required by the standard.
 * @param $unalignedField (string)
 * @param $howManyChars (numeric value)
 * @param $side (string)
 * @param $char (string)
 * @return string
 */
function alignOneField ($unalignedField, $howManyChars, $side, $char) {
  $lengthOfField = strlen($unalignedField);
  $charsAtTheBeginning = $howManyChars - $lengthOfField;
  $alignedField = [];

  if($lengthOfField === $howManyChars) {
    return $unalignedField;
  } else if ($lengthOfField > $howManyChars) {
    throw new \Exception("La cantidad de caracteres ingresada en la hoja de excel supera a la permitida. Dato ingresado: {$unalignedField}");
  }

  if($side === 'left') {
    $alignedField = $unalignedField;
    for($i = $lengthOfField; $i < $howManyChars; $i++) {
      $alignedField = $alignedField . $char;
    }
  } else if($side === 'right') {
    $alignedField = $char;
    for($i = 1; $i < $charsAtTheBeginning; $i++) {
      $alignedField = $alignedField . $char;
    }
    $alignedField = $alignedField . $unalignedField;
  } else if($side === 'none') {
    return $unalignedField;
  }

  return $alignedField;
}

/**
 * Description: Receives an array with a row of values that don't match the standard format and aligns them to match it.
 * @param $unalignedFields (array)
 * @param $section (string)
 * @return array
 */
function alignFields($unalignedFields, $section) {
  // each asociated array has: [amount of characters, side of alignment, character for filling]
  $headArr = [
    'tipoRegistro' => [2,'none','none'],
    'fechaPresentacion' => [8,'none','none'],
    'cantRegistros' => [5,'right','0'],
    'importeTotal' => [19,'right','0']
  ];

  $detailArr = [
    'tipoRegistro2' => [2,'none','none'],
    'servicio' => [10,'left',' '],
    'partida' => [22,'left',' '],
    'cbu' => [22,'none','none'],
    'fechaVencimiento' => [8,'none','none'],
    'importeDebito' => [16,'right','0'],
    'identificacionDebito' => [16,'left',' '],
    'nombreAdherente' => [30,'left',' '],
    'referenciaEmpresa' => [50,'left',' ']
  ];

  $arrCharCount = [];
  $alignedFields = [];
  $i = 0;

  if($section === 'header') {
    $arrCharCount = $headArr;
  } else if ($section === 'detail') {
    $arrCharCount = $detailArr;
  }

  foreach($arrCharCount as $fieldDetails) {
    $alignedFields[$i] = alignOneField($unalignedFields[$i], $fieldDetails[0], $fieldDetails[1], $fieldDetails[2]);
    $i++;
  }

  return $alignedFields;
}

/**
 * Description: Places filler required by the standard in the 13th position.
 * We make use of this function to abstract this logic from the users.
 * @param $alignedFields (reference of array)
 * @param $spaceChar (char)
 */
function putFiller(&$alignedFields, $spaceChar) {
  array_splice($alignedFields, 12, 0, $spaceChar);
  return;
}

/**
 * Description: Puts together all the values in the '$alignedFields' array. Making the string to insert in the txt file.
 * @param $alignedFields (string)
 * @return string
 */
function makeLine($alignedFields) {
  $txt = implode($alignedFields);
  $txt = $txt . "\r\n";
  return $txt;
}

/**
 * Description: Makes the header to be inserted in the returned txt file following the interface standard.
 * @param $worksheet (worksheet phpSpreadSheet object)
 * @return string
 */
function makeHeader(Worksheet $worksheet) {
  $row = 2;
  $rowValues = [];

  for($col = 1, $i = 0; $col <= 4; $col++, $i++) {
    $cell = $worksheet->getCellByColumnAndRow($col, $row);
    $rowValues[$i] = $cell->getValue();
  }

  $alignedFields = alignFields($rowValues, 'header');

  return makeLine($alignedFields);
}

/**
 * Description: Make full array of description. With one value of array per register to write on the txt file.
 * @param $worksheet (worksheet phpSpreadSheet object)
 * @return array
 * @throws \PhpOffice\PhpSpreadsheet\Exception
 */
function makeDescription(Worksheet $worksheet) {
  //get the number of the last row
  $lastRow = $worksheet->getHighestRow();
  $lastRow = intval($lastRow);
  $lines = [];
  $i = 0;

  for($row = 4; $row <= $lastRow; $row++, $i++) {
    $rowValues = getRowValues($worksheet, $row);
    $alignedFields = alignFields($rowValues, 'detail');
    putFiller($alignedFields, ' ');
    $lines[$i] = makeLine($alignedFields);
  }

  return $lines;
}

/**
 * Description: Puts header and description in the txt file.
 * @param $file (file stream)
 * @param $header (string)
 * @param $details (array)
 */
function putInFile($fileName, $header, $details) {
  $myFile = fopen($fileName, "w") or die("Unable to open file!");

  $detailsLength = count($details);

  fwrite($myFile, $header);
  for($i = 0; $i < $detailsLength; $i++) {
    fwrite($myFile, $details[$i]);
  }

  fclose($myFile);
}

/**
 * @param $excelFileName
 * @return \PhpOffice\PhpSpreadsheet\Worksheet\Worksheet
 * @throws \PhpOffice\PhpSpreadsheet\Exception
 * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
 */
function getWorksheet($excelFileName) {
  if(file_exists($excelFileName . '.xls')) {
    $excelFileName = $excelFileName . '.xls';
  } else if(file_exists($excelFileName . '.xlsx')) {
    $excelFileName = $excelFileName . '.xlsx';
  } else if(file_exists($excelFileName . '.ods')) {
    $excelFileName = $excelFileName . '.ods';
  }

  $excelReader = IOFactory::createReaderForFile($excelFileName);
  $excelObj = $excelReader->load($excelFileName);

  return $excelObj->getActiveSheet();
}

try {
  $excelFileName = 'datos_debito_directo';
  $worksheet = getWorksheet($excelFileName);
  $header = makeHeader($worksheet);
  $details = makeDescription($worksheet);
} catch (\Exception $exceptionObj) {
  echo 'Error: ', $exceptionObj->getMessage(), "\n";
  die;
}

putInFile("datos_integracion_santander.txt", $header, $details);

echo 'The integration file has been created.';
echo "\r\n";