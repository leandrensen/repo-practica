<?php

namespace app\classes;

use app\classes\GoogleDrive;
use app\models\Order;
use app\models\OrderItem;

class ClientInvoice {
  private $path;
  private $file;
  private $options;
  private $data;
  private $invoice;

  public function __construct($path, $file, $options, $data){
    $this->path    = $path;
    $this->file    = $file;
    $this->options = $options;
    $this->data    = $data;
  }

  public function generate_invoice() {
    $this->invoice = \PHPExcel_IOFactory::load($this->path . $this->file);

    if(!$this->invoice)
      return false;

    $this->write_template();

    $name = $this->data['name'];
    $this->filepath = $this->path . 'generated/' . $name . ".xlsx";

    // Escribe el archivo XLS
    $writer = \PHPExcel_IOFactory::createWriter($this->invoice, 'Excel2007');
    ob_start();
    $writer->save($this->filepath);
    return ob_get_clean();
  }
  
  public function write_template() {
    FileLog::getInstance()->log('[fy] Escritura del Remito [1/2] [/]', "Comienza la escritura del remito del pedido {$this->data['number']}.");

    if(isset($this->options['number']))
      $this->write_number();
    if(isset($this->options['date']) && isset($this->options['requested_date']))
      $this->write_date();
    if(isset($this->options['items']))
      $this->write_items();
    if(isset($this->options['total_price']))
      $this->write_total_price();
    if(isset($this->options['client']))
      $this->write_client();
    if(isset($this->options['delivery_point']))
      $this->write_delivery_point();
    if(isset($this->options['comments']))
      $this->write_comments();

    FileLog::getInstance()->log('[fy] Escritura del Remito [2/2] [/]', "Finalizo la escritura del remito del pedido {$this->data['number']}.");
  }

  private function write_number() {
    FileLog::getInstance()->log('[fy] Escritura del Numero [1/3] [/]', "Se setea la hoja donde debe escribirse el numero del pedido.");
    $this->invoice->setActiveSheetIndex($this->options['number']['sheet']);
    FileLog::getInstance()->log('[fy] Escritura del Numero [2/3] [/]', "Se obtiene la hoja donde debe escribirse el numero del pedido.");
    $sheet = $this->invoice->getActiveSheet();

    $column  = $this->options['number']['column'];
    $row     = $this->options['number']['row'];
    $preText = $this->options['number']['pre_text'];

    FileLog::getInstance()->log('[fy] Escritura del Numero [3/3] [/]', "Se escribe el numero del pedido.");
    $sheet->setCellValue("{$column}{$row}", "{$preText}{$this->data['number']}");
  }

  private function write_date() {
    FileLog::getInstance()->log('[fy] Escritura de la Fecha de Solicitud [1/3] [/]', "Se setea la hoja donde debe escribirse la fecha de solicitud.");
    $this->invoice->setActiveSheetIndex($this->options['requested_date']['sheet']);
    FileLog::getInstance()->log('[fy] Escritura de la Fecha de Solicitud [2/3] [/]', "Se obtiene la hoja donde debe escribirse la fecha de solicitud.");
    $sheet = $this->invoice->getActiveSheet();

    $column  = $this->options['requested_date']['column'];
    $row     = $this->options['requested_date']['row'];
    $preText = $this->options['requested_date']['pre_text'];
    $date    = date($this->options['requested_date']['format'], strtotime($this->data['requested_date']));

    FileLog::getInstance()->log('[fy] Escritura de la Fecha de Solicitud [3/3] [/]', "Se escribe la fecha de solicitud.");
    $sheet->setCellValue("{$column}{$row}", "{$preText}{$date}");

    FileLog::getInstance()->log('[fy] Escritura de la Fecha de Entrega [1/3] [/]', "Se setea la hoja donde debe escribirse la fecha de entrega.");
    $this->invoice->setActiveSheetIndex($this->options['date']['sheet']);
    FileLog::getInstance()->log('[fy] Escritura de la Fecha de Entrega [2/3] [/]', "Se obtiene la hoja donde debe escribirse la fecha de entrega.");
    $sheet = $this->invoice->getActiveSheet();

    $column  = $this->options['date']['column'];
    $row     = $this->options['date']['row'];
    $preText = $this->options['date']['pre_text'];
    $date    = date($this->options['date']['format'], strtotime($this->data['date']));

    FileLog::getInstance()->log('[fy] Escritura de la Fecha de Entrega [3/3] [/]', "Se escribe la fecha de entrega.");
    $sheet->setCellValue("{$column}{$row}", "{$preText}{$date}");
  }

  private function write_items() {
    $totalIteration = count($this->data['items']) + 2;
    FileLog::getInstance()->log('[fy] Escritura de Items [1/' . $totalIteration . '] [/]', "Se setea la hoja donde deben escribirse los items del pedido.");
    $this->invoice->setActiveSheetIndex($this->options['items']['sheet']);
    FileLog::getInstance()->log('[fy] Escritura de Items [2/' . $totalIteration . '] [/]', "Se obtiene la hoja donde debe escribirse los items del pedido.");
    $sheet = $this->invoice->getActiveSheet();
    
    $firstLine   = $this->options['items']['first_line'];
    $firstColumn = $this->options['items']['first_column'];
    
    $row              = $firstLine;
    $currentIteration = 3;

    foreach($this->data['items'] as $item){
      foreach($this->options['items']['columns'] as $key => $value){
        if(count($value) == 1 && !is_array($value[0])){
          $element = $item->$value[0];
        }else{
          if (is_array($value[0])) {
            $elementRender = [];
            foreach ($value as $subValue) {
              if(count($subValue) == 1){
                $elementRender[] = $item->$subValue[0];
              } else {
                $element = $item;
                
                foreach($subValue as $index){
                  $element = $element->$index;
                }

                $elementRender[] = $element;
              }
            }

            $element = implode('. ', $elementRender);
          } else {
            $element = $item;
            foreach($value as $index){
              $element = $element->$index;
            }
          }
        }

        FileLog::getInstance()->log('[fy] Escritura de Items [' . $currentIteration . '/' . $totalIteration . '] [/]', "Se escribe un item.");
        $sheet->setCellValue("{$key}{$row}", $element);
      }

      $row++;
    }
  }

  private function write_total_price() {
    FileLog::getInstance()->log('[fy] Escritura del Monto Total [1/3] [/]', "Se setea la hoja donde debe escribirse el monto total.");
    $this->invoice->setActiveSheetIndex($this->options['total_price']['sheet']);
    FileLog::getInstance()->log('[fy] Escritura del Monto Total [2/3] [/]', "Se obtiene la hoja donde debe escribirse el monto total.");
    $sheet = $this->invoice->getActiveSheet();

    $column     = $this->options['total_price']['column'];
    $row        = $this->options['total_price']['row'];
    $format     = $this->options['total_price']['format'];
    $totalPrice = number_format($this->data['total_price'], $format['decimals'], $format['decimal'], $format['thousand']);
    $currency   = $this->data['currency']->symbol;

    FileLog::getInstance()->log('[fy] Escritura del Monto Total [3/3] [/]', "Se escribe el monto total.");
    $sheet->setCellValue("{$column}{$row}", "{$currency} {$totalPrice}");
  }

  private function write_client() {
    FileLog::getInstance()->log('[fy] Escritura del Cliente [1/3] [/]', "Se setea la hoja donde debe escribirse el cliente.");
    $this->invoice->setActiveSheetIndex($this->options['client']['sheet']);
    FileLog::getInstance()->log('[fy] Escritura del Cliente [2/3] [/]', "Se obtiene la hoja donde debe escribirse el cliente.");
    $sheet = $this->invoice->getActiveSheet();

    $column     = $this->options['client']['column'];
    $row        = $this->options['client']['row'];

    FileLog::getInstance()->log('[fy] Escritura del Cliente [3/3] [/]', "Se escribe el cliente.");
    $sheet->setCellValue("{$column}{$row}", $this->data['client']->name);
  }

  private function write_delivery_point() {
    FileLog::getInstance()->log('[fy] Escritura del Punto de Entrega [1/3] [/]', "Se setea la hoja donde debe escribirse el punto de entrega.");
    $this->invoice->setActiveSheetIndex($this->options['delivery_point']['sheet']);
    FileLog::getInstance()->log('[fy] Escritura del Punto de Entrega [2/3] [/]', "Se obtiene la hoja donde debe escribirse el punto de entrega.");
    $sheet = $this->invoice->getActiveSheet();

    $column = $this->options['delivery_point']['column'];
    $row    = $this->options['delivery_point']['row'];

    FileLog::getInstance()->log('[fy] Escritura del Punto de Entrega [3/3] [/]', "Se escribe el punto de entrega.");
    $sheet->setCellValue("{$column}{$row}", $this->data['delivery_point']->address);
  }
  
  private function write_comments() {
    FileLog::getInstance()->log('[fy] Escritura de los Comentarios [1/3] [/]', "Se setea la hoja donde deben escribirse los comentarios.");
    $this->invoice->setActiveSheetIndex($this->options['comments']['sheet']);
    FileLog::getInstance()->log('[fy] Escritura de los Comentarios [2/3] [/]', "Se obtiene la hoja donde debe escribirse los comentarios.");
    $sheet = $this->invoice->getActiveSheet();

    $column  = $this->options['comments']['column'];
    $row     = $this->options['comments']['row'];

    FileLog::getInstance()->log('[fy] Escritura de los Comentarios [3/3] [/]', "Se escriben los comentarios.");
    $sheet->setCellValue("{$column}{$row}", "{$this->data['comments']}");
  }
}

