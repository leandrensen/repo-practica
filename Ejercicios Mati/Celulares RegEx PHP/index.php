<?php 
require_once("singletonDB.php");
require_once("regExCel.php");
$error = '';
$caracteristica = '';
$numero = '';

if(isset($_POST) && !empty($_POST)){
    $caracteristica = $_POST["caracteristica"];
    $numero = $_POST["numero"];

    // echo "tipo de caracteristica: ".gettype ($caracteristica)."<br>";
    // echo "tipo de caracteristica: ".gettype ($numero)."<br>";

    if(validarCaracteristica($_POST["caracteristica"]) && validarNumero($_POST["numero"])){
        $instancia = SingletonDB::getInstance();
        // echo "Lo metemos en la base";
        try{
            $sql = "INSERT INTO partners(name, email, product_list_updated_at, number_phone,code_phone)
                    VALUES ('TEST','TEST', '2017/09/03',$numero, $caracteristica)";
            // use exec() because no results are returned
            $instancia->conn()->exec($sql);
            echo "New record created successfully";
        }          
        catch(PDOException $e){
            echo $sql . "<br>" . $e->getMessage();
        }
    }
    else{
      $error = 'error';
    }
}

?>
<!-- 
<?php
 
?> -->

<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title>Ejemplo PHP MySQLi POO MVC</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <style>
            input{
                margin-top:5px;
                margin-bottom:5px;
            }
            .right{
                float:right;
            }

            .error{
                border: 1px solid red;
            }
        </style>
    </head>
    <body>
        <form action="index.php" method="post" class="col-lg-6">
            <h3>Introducir Teléfono</h3>
            <hr/>
            Caracteristica: <input type="text" value="<?= $caracteristica; ?>" name="caracteristica" placeholder="Ej: 0232 / 232 / 011 / 11 " class="form-control <?= $error; ?>"/>    
            Número: <input type="text" value="<?= $numero; ?>" name="numero" placeholder="Ej: 47449874 / 68386451" class="form-control <?= $error; ?>"/>      
            <input type="submit" value="enviar" class="btn btn-success"/>
            <?php if($error == 'error'): ?>
                Hubo un error
            <?php endif; ?>
        </form>
         
      <!--   <div class="col-lg-7">
            <h3>Usuarios</h3>
            <hr/>
        </div> -->
      <!--   <section class="col-lg-6 usuario" style="height:400px;overflow-y:scroll;">
            <form action="regExCel2.php" method="post">
                <h3>Introducir Número</h3>
                <hr/>
                Número: <input type="text" name="numero" placeholder="Ej: 47449874 / 68386451" class="form-control"/>          
                <input type="submit" value="enviar" class="btn btn-success"/>
            </form>
            <!-- <hr/>  -->       
        <!--</section> -->
        <footer class="col-lg-12">
            <hr/>
           Ejemplo ABM Extendeal de Números de Teléfono.
        </footer>
    </body>
</html>
