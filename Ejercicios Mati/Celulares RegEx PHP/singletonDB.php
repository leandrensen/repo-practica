<?php

class SingletonDB
{   
    private static $instancia;
    private $db;
    private $servername = "localhost";
    private $username = "root";
    private $password = "extendeal";
    private $myDB = "extendeal_ar";      

    private function __construct(){
     echo "mysql:host=$servername;dbname=$myDB";
     try {
         $this->db = new \PDO("mysql:host=$this->servername;dbname=$this->myDB", $this->username, $this->password);
         // set the PDO error mode to exception
         $this->db->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
         echo "Connected successfully"; 
         }
     catch(\PDOException $e)
         {
         echo "Connection failed: " . $e->getMessage();
         }     
    }

    public static function getInstance(){
        if (!self::$instancia instanceof self){
           self::$instancia = new self;
        }
        return self::$instancia;
    }

    public function conn(){
      return $this->db;
    }

   // para que no haya duplicados vvvv   

   public function __clone(){
      trigger_error("Operación Invalida: No puedes clonar una instancia de ". get_class($this) ." class.", E_USER_ERROR );
   }

   public function __wakeup(){
      trigger_error("No puedes deserializar una instancia de ". get_class($this) ." class.");
   }
}
?>
