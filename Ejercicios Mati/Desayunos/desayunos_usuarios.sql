-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: desayunos
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.30-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` text COLLATE utf8_spanish2_ci,
  `fecha_ingreso` date DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `area` varchar(45) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'Romano Matías Agustín Roberto','2016-08-01','1975-02-05','Sistemas'),(2,'Villalonga Nicolas','2016-10-20','1990-08-10','Sistemas'),(3,'Leites Ezequiel','2016-11-21','1996-05-25','Sistemas'),(4,'Liendo Ernesto Cirilo','2017-03-16','1987-01-31','Sistemas'),(5,'Ferrero Franco','2017-04-03','1992-07-15','Sistemas'),(6,'Pasquini Alejandro Norberto','2017-05-11','1993-10-31','Sistemas'),(7,'Mijares Bellatrix','2017-07-21','1994-09-26','Sistemas'),(8,'Mendez Leandro Marcelo','2017-07-24','1989-05-20','Sistemas'),(9,'Zamora Utrera Roberto Rafael','2017-08-04','1989-12-14','Sistemas'),(10,'Saleh Alaa','2017-08-14','1991-10-10','Sistemas'),(11,'Uso Medina Manuel Enrique','2017-09-07','1985-07-22','Sistemas'),(12,'Paz Monsalve Alvaro Joel','2017-09-07','1991-12-07','Sistemas'),(13,'Peluso Luciano Martin','2017-09-12','1988-01-20','Sistemas'),(14,'Jimenez Pablo','2017-09-13','1991-07-05','Sistemas'),(15,'Barzola Huatuco Luis Rolando','2017-09-18','1985-11-22','Sistemas'),(16,'Dutten Cesar Manuel','2017-09-18','1998-02-20','Sistemas'),(17,'Seijo Lisandro','2017-11-16','1989-01-04','Sistemas'),(18,'Salaris Soledad','2017-11-27','1991-12-03','Sistemas'),(19,'Ibañez Germán Esteban ','2018-01-16','1980-09-09','Sistemas'),(20,'Skverer Ariel','2018-02-01','1950-09-09','Sistemas');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-01 21:20:47
