<?php
class Partner extends EntidadBase{
    private $id;
    private $nombre;
    private $email;
    private $alternative_email;
    private $date;
    private $code_phone;
    private $number_phone;
     
    public function __construct() {
        $table="partners";
        parent::__construct($table);
    }
     
    public function getId() {
        return $this->id;
    }
 
    public function setId($id) {
        $this->id = $id;
    }
     
    public function getNombre() {
        return $this->nombre;
    }
 
    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }  
 
    public function getEmail() {
        return $this->email;
    }
 
    public function setEmail($email) {
        $this->email = $email;
    } 

    public function getEmailAlternativo() {
        return $this->email;
    }
    
    public function setEmailAlternativo($alternative_email) {
        $this->alternative_email = $alternative_email;
    } 

    public function getDaysInAdvance() {
        return $this->date;
    }
    
    public function setDaysInAdvance($date) {
        $this->date = $date;
    }

    public function getCodPhone() {
        return $this->code_phone;
    }
    
    public function setCodPhone($code_phone) {
        $this->code_phone = $code_phone;
    }

    public function getNumPhone() {
        return $this->number_phone;
    }
    
    public function setNumPhone($number_phone) {
        $this->number_phone = $number_phone;
    }
 
 
    public function save(){
        try{
            $query="INSERT INTO partners(name, email, alternative_email, product_list_updated_at, number_phone,code_phone)
                    VALUES('".$this->nombre."','".$this->email."','".$this->alternative_email."','".$this->date."','".$this->number_phone."','".$this->code_phone."');";
                                
            $save=$this->db()->exec($query);
        }
        catch(PDOException $e){
            echo $sql . "<br>" . $e->getMessage();
        }
        
        return $save;
    }
 
}
?>
