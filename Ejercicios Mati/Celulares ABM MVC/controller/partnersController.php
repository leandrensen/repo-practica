<?php
define('__ROOT__', dirname(dirname(__FILE__))); 
require_once(__ROOT__.'/helper/phoneValidator.php'); 

class PartnersController extends ControladorBase{

    // define variables and set to empty values
    public $nameErr = "";
    public $emailErr = "";
    public $altEmailErr = "";
    public $carErr = "";
    public $numErr = "";
         
    public function __construct() {
        parent::__construct();
    }
     
    public function index(){
         
        //Creamos el objeto partner
        $partner=new Partner();
         
        //Conseguimos todos los usuarios
        $allpartners=$partner->getAll();
        
        //Cargamos la vista index y le pasamos valores
        $this->view("index",array(
            "allpartners"=>$allpartners,
            "Hola"    =>"Soy Víctor Robles"
        ));

        // $this->view("index",$allpartners);
    }
     
    public function crear(){
        if(PhoneValidator::validar($_POST["caracteristica"],$_POST["numero"])) {
            if(isset($_POST["nombre"]) && !empty($_POST["nombre"])) {
                //Creamos un partner
                //Por que se crea aca y no en modelos ?
                // El controaldor no era para pasar informacion entre vistas y modelos ? 
                $partner=new Partner();
                $partner->setNombre($_POST["nombre"]);
                $partner->setEmail($_POST["email"]);
                $partner->setEmailAlternativo($_POST["email_alternativo"]);
                $partner->setDaysInAdvance('2017/09/03');
                $partner->setCodPhone($_POST["caracteristica"]);
                $partner->setNumPhone($_POST["numero"]);            
                $save=$partner->save();                      
            }
            else{
                $nameErr = "El campo nombre debe estar completo";
            }
        }
        else{
            
        }
        // elseif(PhoneValidator::validarCaracteristica($_POST["caracteristica"])) {
        //         $carErr = "Error al ingresar caracteristica";
        //     }
        //     elseif(PhoneValidator::validarNumero($_POST["numero"])) {
        //         $numErr = "Error al ingresar numero";
        //     }  
        //     elseif(empty ($_POST["caracteristica"])) {
        //         $carErr = "El campo caracteristica debe estar completo";
        //     }
        //     elseif(empty ($_POST["numero"])) {
        //         $numErr = "El campo numero debe estar completo";
        //     }        
                
        $this->redirect("Partners", "index");
    }
     
    public function borrar(){
        if(isset($_GET["id"])){ 
            $id=(int)$_GET["id"];
             
            $partner=new Partner();
            $partner->deleteById($id); 
        }
        $this->redirect();
    }     

    public function hola(){
        $usuarios=new UsuariosModel();
        $usu=$usuarios->getUnUsuario();
        var_dump($usu);
    } 
}
?>
