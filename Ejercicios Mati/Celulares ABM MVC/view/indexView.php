<!DOCTYPE HTML>
<html lang="es">
    <head>
        <meta charset="utf-8"/>
        <title>ABM Telefonos Extendeal</title>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <style>
            input{
                margin-top:5px;
                margin-bottom:5px;
            }
            .right{
                float:right;
            }
        </style>
    </head>
    <body>
        <form action="<?php echo $helper->url("partners","crear"); ?>" method="post" class="col-lg-5">
            <h3>Añadir Proveedor(telefonos)</h3>
            <hr/>
            Nombre: <input type="text" name="nombre" value="<?php echo $nombre; ?>" class="form-control"/><span class="error"> <?php echo $nameErr;?></span>
            Email: <input type="text" name="email" value="<?php echo $email; ?>" class="form-control"/><span class="error"> <?php echo $emailErr;?></span>
            Email Alternativo: <input type="text" name="email_alternativo" value="<?php echo $email_alternativo; ?>" class="form-control"> <span class="error"> <?php echo $altEmailErr;?></span>
            Característica: <input type="text" name="caracteristica" placeholder="Ej: 0232 / 232 / 011 / 11" value="<?php echo $caracteristica; ?>" class="form-control"/> <span class="error"> <?php echo $carErr;?></span>   
            Número: <input type="text" name="numero" placeholder="Ej: 47449874 / 68386451" value="<?php echo $numero; ?>" class="form-control"/> <span class="error"> <?php echo $numErr;?></span>   
            <input type="submit" value="enviar" class="btn btn-success"/>
        </form>

        <div class="col-lg-7">
            <h3>Proveedores</h3>
            <hr/>
        </div>
        <section class="col-lg-7 usuario" style="height:400px;overflow-y:scroll;">
                
            <?php foreach($allpartners as $partner) { //recorremos el array de objetos y obtenemos el valor de las propiedades ?>
                <?php echo $partner['id']; ?> -
                <?php echo $partner['name']; ?> -
                <?php echo $partner['email']; ?> -
                <?php echo $partner['alternative_email'] ?> -   
                <?php echo $partner['code_phone']; ?> -
                <?php echo $partner['number_phone']; ?>
                <div class="right">
                    <a href="<?php echo $helper->url("partners","borrar"); ?>&id=<?php echo $partner['id']; ?>" class="btn btn-danger">Borrar</a>
                </div>
                <hr/>
            <?php } ?>
        </section>
        <footer class="col-lg-12">
            <hr/>
           ABM Extendeal de Números de Teléfono.
        </footer>
    </body>
</html>
    
