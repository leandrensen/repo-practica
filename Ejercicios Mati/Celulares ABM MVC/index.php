<?php
//Configuración global
require_once 'config/global.php';

//Funciones para el controlador frontal
require_once 'core/controladorFrontal.func.php';

//Base para los controladores
require_once 'core/controladorBase.php';

//Cargamos controladores y acciones
if(isset($_GET["controller"])){
    $controllerObj=cargarControlador($_GET["controller"]);
    lanzarAccion($controllerObj);
}else{
    $controllerObj=cargarControlador(CONTROLADOR_DEFECTO);
    lanzarAccion($controllerObj);
}
?>
