// var a = 1;
// var b = "Leandro";

// alert (a);

// console.log(a);
// console.warn(a);
// console.error(a);
// console.info(a);

// console.log(a + a);




// function imprimir () {
//   for(var i = 0;i < 8000; i++)   {
//     console.log("Imprimo");
//   }
// }

// function presionoClick() {
//   console.log("Click en Boton");
// }

// imprimir();




// // notaciones de punto y corchetes

// var persona = { 
//   nombre: "Juana",
//   apellido: "Herrera",
//   edad: 25,
//   direccion: {
//     pais: "Costa Rica",
//     ciudad: "San José",
//     edificio: {
//       nombre: "Edificio principal",
//       telefono: "2222-3333"
//     }
//   }
// };

// console.log(persona.nombre);
// console.log(persona.direccion);
// console.log(persona.direccion.pais);

// persona.direccion.zipcode = 11101;
// console.log(persona.direccion);
// console.log(persona.direccion.zipcode);


// console.log(persona.direccion.edificio.telefono);


// var edificio = persona.direccion.edificio; // la var edificio apunta a la direccion de memoria del objeto edificio

// edificio.nopiso = "8vo piso";

// console.log(persona);


// console.log(persona["nombre"]);
// console.log(persona["direccion"]["pais"]);

// var campo = "edad2";

// console.log(persona[campo]);
// console.log(persona.edad2);


// //funciones

// var a = 30;

// function primeraFuncion(){
//   // var a = 20;

//   console.log(a);
// }

// primeraFuncion();

// var miFuncion = primeraFuncion; // 'miFuncion' apunta a la direccion de memoria de 'primeraFuncion()'. O sea que miFuncion() se puede ejecutar como 'primeraFuncion()'.




// // argumento de funciones

// function imprimir (nombre, apellido) {
//   if(apellido === undefined) {
//     apellido = "natalia";
//   }

//   // apellido = apellido || "natalia"; //es lo mismo que arriba, si apellido da undefined, elige 'natalia', si no e sun define, asigna apellido.

//   if(nombre === undefined) {
//     nombre = "natalia";
//   }

//   // nombre = nombre || "natalia";

//   console.log(nombre + " " + apellido);
// }

// imprimir("Juan", "Gomez");
// imprimir("Juan");
// imprimir ();

// // funciones con objetos

// function imprimir (persona) {
//   console.log(persona.nombre + " " + persona.apellido);

//   persona.nombre = "Maria";
// }

// imprimir({ // le pasas el objeto entre '{ }'
//   nombre: "Juan",
//   apellido:"Padilla"
// });

// // es lo mismo que hacer esto

// var obj = { 
//   nombre: "Juan",
//   apellido:"Padilla"
// };

// imprimir(obj);

// console.log(obj);


// // mandar funciones como argumentos a una funcion

// function imprimir (fn) { // FUNCION ANÓNIMA
//   fn();
// };

// var persona = {
//   nombre: "Juan",
//   apellido: "Padilla"
// };

// imprimir(
//   function() {
//     console.log("Funcion anónima");
//   }
// );


// var miFuncion = function() { // FUNCIÓN EXPLÍCITA
//   console.log("miFuncion");
// }

// imprimir(miFuncion);

// retorno de funciones
// function obtenerAleatorio() {
//   return Math.random();
// }

// console.log(obtenerAleatorio() + 10);

// function obtenerNombre(){
//   return "Juan";
// }

// console.log(obtenerNombre() + " " + "Padilla");

// var nombre = obtenerNombre();

// console.log(nombre);

// function esMayor05() {
//   if(obtenerAleatorio() > 0.5) {
//     return true;
//   } else {
//     return false;
//   }
// }


// if(esMatyor05()) {
//   console.log("Es mayor a 0.5");
// }else {
//   console.log("Es menor a 0.5");
// }

// // funcion retorna objeto
// function crearPersona(nombre, apellido){
//   // var obj = {};
//   // return obj;
//   //es lo mismo que poner esto:
//   return{
//     nombre: nombre,
//     apellido: apellido
//   };
// }

// var persona = crearPersona("Maria","Paz");
// console.log(persona.nombre); 
// console.log(persona.apellido);

// //funcion que regresa funcion
// function creaFuncion() {
//   return function(nombre) {
//     console.log('Me creo ' + nombre);

//     return function() {
//       console.log('Segunda funcion');
//     }
//   }
// }

// var nuevaFuncion = creaFuncion();
// var segundaFuncion = nuevaFuncion(persona.nombre);

// segundaFuncion();
 

// // clase 15
// //funciones de primera clase. Las funciones son objetos.

// function a(){ 
//   console.log('funciòn a');
// }

// a();

// a.nombre = "Maria";
// a.direccion = {
//   pais: 'Costa Rica',
//   ciudad: 'San Jose',
//   edificio: {
//     piso: '8vo',
//     nombre: 'Edificio principal'
//   }
// }


// // 16 methodos y THIS

// var persona = {
//   nombre:"Maria",
//   apellido: "Dubon",
//   imprimirNombre: function() {
//     console.log(this.nombre + ' ' + this.apellido);
//   },
//   direccion:{
//     pais: 'Costa Rica',
//     obtenerPais:function(){
//       var self = this;
//       var nuevaDireccion = function() {
//         console.log(self);
//       }
//       console.log('La direcciòn es en ' + self.pais);
//     }
//   }
// };

// persona.nombre = 'Andres';

// persona.imprimirNombre();
// persona.direccion.obtenerPais();


// 17. Palabra reservada 'new'

// Persona carlos = new Persona();

// var carlos = new Persona();

// var juan = {};

// console.log(juan);

// function Persona(){
//   this.nombre = 'Juan';
//   this.apellido = 'Mendoza';
//   this.edad = 30;

//   console.log('Pasó por aquí');
// }

// var juan = new Persona();
// console.log(juan);



// // Juego LOTR 

// //Creo clase jugador 

// function Jugador(nombre) { //La letra mayuscula en jugador indica que es una funcion de primera clase. Es un objeto.

//   this.nombre = nombre;
//   this.pv = 100;
//   this.sp = 100;

//   this.curar = function(jugadorObjetivo) { 
//     if(this.sp >= 40) {
//       // jugadorObjetivo.pv = jugadorObjetivo.pv + 20;
//       jugadorObjetivo.pv += 20;
//       this.sp -= 40;  
//     } else {
//       console.info(this.nombre + ' no tiene sp');
//     }    
//     this.estado(jugadorObjetivo);
//   }

//   this.tirarFlecha = function(jugadorObjetivo) {
//     if(jugadorObjetivo.pv > 40) {
//       jugadorObjetivo.pv -= 40;  
//     }else {
//       jugadorObjetivo.pv = 0;
//       console.error(jugadorObjetivo.nombre + ' muriò.');
//     }
//     this.estado(jugadorObjetivo);
//   }

//   this.ataqueEspada = function(jugadorObjetivo) {
//     if(jugadorObjetivo.pv > 50) {
//       jugadorObjetivo.pv -= 50;  
//     }else {
//       jugadorObjetivo.pv = 0;
//       console.error(jugadorObjetivo.nombre + ' muriò.');
//     }
//     this.estado(jugadorObjetivo);
//   }

//   this.estado = function(jugadorObjetivo) {
//     console.info(this);
//     console.info(jugadorObjetivo);
//   }
// }

// //instancio clase en objetos del juego
// var gandalf = new Jugador('Gandalf');
// var legolas = new Jugador('Legolas');
// var aragorn = new Jugador('Aragorn');


// //testeo
// console.log(gandalf);
// console.log(legolas);
// console.log(aragorn);


// //.19 Prototypes

// function Persona() {
//   this.nombre = 'Fernando';
//   this.apellido = 'Herrera';
//   this.edad = 30;
//   this.pais = 'Costa Rica';
// }

// //Prototype se usa para no repetir informacion
// //Si tenemos un millòn de instancias del objeto persona, una funcion prototipada como en este ejemplo, no se instancia un millon de veces. Se instancia una sola.
// //cuando se llama al metodo, js primero busca dentro del objeto, (this.metodo()), despues, si no la encontrò, la busca en el prototype
// Persona.prototype.imprimirInfo = function() {
//     console.log(this.nombre + ' ' + this.apellido + '(' + this.edad + ')');
// }

// var fer = new Persona();

// // fer.imprimirInfo();
// // console.log(fer); 
// // console.log(fer.pais); 

// var a = 'Fernando';


// Number.prototype.esPositivo = function() {
//   if(this > 0) {
//     return true;
//   } else {
//     false;
//   }
// }




// // .20 Funciones anònimas

// (function(){
//   var a = 10;

//   console.log(a);

//   function cambiarA(){
//     a = 20;
//   }

//   cambiarA();

//   console.log(a);
// })();


// // otro tipo de funcion anonimo

// function ejecutarFuncion(fn){
//   if(fn() === 1){
//     return true;
//   }else{
//     return false;
//   }
// };

// console.log(
//     ejecutarFuncion(function(){
//       console.log('Funcion anònima ejecutada');
//       return 1;
//     })
// );




// //.22 areglos


// // var arr = new Array(); Es lo mismo que:

// var arr = [5,4,3,2,1];

// console.log(arr);

// console.log(arr[0],arr[4],arr[5]);

// arr.reverse();
// console.log(arr);

// arr = arr.map(function(elem){
//   elem *= elem;
//   return elem;
// });

// console.log(arr);


// arr = arr.map(Math.sqrt);
// console.log(arr);

// arr = arr.join('|');
// console.log(arr);

// arr = arr.split('|');
// console.log(arr);

// arr.push('6');
// console.log(arr);

// arr.unshift('0');
// console.log(arr);

// console.log(arr.toString());

// var elimine = arr.pop();
// console.log(arr, elimine);

// arr.splice(1, 1, '10', '20', '30');
// console.log(arr);

// arr = arr.slice(2,3);
// console.log(arr);

// //.23 Arreglos - parte 2 

// var arr = [
//   true,
//   {
//     nombre: "Fernando",
//     apellido: "Herrera"
//   },
//   function(){
//     console.log("Estoy viviendo en un arreglo.");
//   },
//   function(persona){
//     console.log(persona.nombre + ' ' + persona.apellido);
//   },
//   [
//     'Nico',
//     'Alaa',
//     'Eze',
//     'Lea',
//     [
//       'Ale',
//       'Mati',
//       'Dani',
//       function(){
//         console.log(this);
//       }
//     ]
//   ]
// ];

// console.log(arr);
// console.log(arr[0]);
// console.log(arr[1].nombre + ' ' +arr[1].apellido);

// arr[2]();

// arr[3](arr[1]);

// console.log(arr[4][4][1]);

// var arreglo2 = arr[4][4];

// console.log(arreglo2);

// arreglo2[1] = 'Matiiii';

// console.log(arreglo2);
// console.log(arr);

// arr[4][4][3]();



// // .24 Argumentos

// var arguments = 10;

// function miFuncion(a,b,c,d){
//   // console.log(arguments);
// if(arguments.length !== 4){
//   console.error('La función necesita 4 parametros');
//   alert('La función necesita 4 parametros');
//   return;
// }

//   console.log(a + b + c + d);
// }

// miFuncion(10,20,30,40);



//.25 Carga de operadores 

// //.26 Polimorfismo

// function determinaDato(a){
//   if(a === undefined){
//     console.log('a es undefined... no se que hacer');
//   }
//   if(typeof a === 'number'){
//     console.log('a es un numero, y puedo hacer operaciones con numeros');
//   }
//   if(typeof a === 'string'){
//     console.log('a es un texto, y puedo hacer operaciones con textos');
//   }
//   if(typeof a === 'object'){
//     console.log('a es un objeto... pero puede ser cualquier cosa...');
//     if(a instanceof Number) {
//       console.log('a es un objeto numerico...');
//     }
//   }
// }

// var b = new Number(3);

// console.log(b);

// determinaDato(b); // cuando lo definimos asi ^^ con new, el 3 no es number, es un objeto.




// // .27 funciones y sus contenidos

// function crearFunciones() {
//   var arr = [];
//   var numero = 1;

//   for(var numero = 1; numero <= 3; numero++){
//     arr.push(
//       (function(numero){
//         return function(){
//           console.log(numero);
//         }
//       })(numero)
//     );
//   }

//   return arr;
// }

// //   arr.push(
// //     (function(numero){
// //       return function(){
// //         console.log(numero);
// //       }
// //     })(numero)

// //   );  

// //   var numero = 2;

// //   arr.push(function(){
// //     console.log(numero);
// //   });

// //   var numero = 3;

// //   arr.push(function(){
// //     console.log(numero);
// //   });

// //   numero = 10;
// //   return arr;
// // }

// var funciones = crearFunciones();

// funciones[0]();
// funciones[1]();
// funciones[2]();
// funciones[3]();
// funciones[4]();


// //.31 objeto fecha

// var hoy = new Date();
// var fMili = new Date(0);
// // var fFija = new Date( anio, mes, dia, hora, min, seg, mili);
// var fFija = new Date(2017, 9, 5, 14, 50, 08, 08)

// console.log(hoy);
// console.log(fMili);
// console.log(fFija);

// console.log(hoy.getFullYear() + 1);
// console.log(hoy.getDate());
// console.log(hoy.getHours());
// console.log(hoy.getMilliseconds());
// console.log(hoy.getMonth());
// console.log(hoy.getSeconds());

// //ver cuanto demorò un proceso

// var inicio = new Date();

// for(i=0; i<15000; i++) {
//   console.log('Algo...');
// }

// var fin = new Date();

// // console.log(inicio);
// // console.log(fin);

// var duracion = fin.getTime() - inicio.getTime();
// console.log(duracion + 'milisegundos.');
// console.log(duracion/1000 + 'segundos.');


// // .32 Operaciones con fechas

// var fecha = new Date(2016, 9, 20);

// Date.prototype.sumarDias = function(dias) {
//   this.setDate(this.getDate() + dias);
//   return this;
// }

// Date.prototype.sumarAnios = function(anios) {
//   this.setFullYear(this.getFullYear() + anios);
//   return this;
// }

// console.log(fecha);
// console.log(fecha.sumarDias(-20));
// console.log(fecha.sumarAnios(-16));


// // 42. Cookies
// function crearCookie(nombre,valor){
//   valor = escape(valor); // escapa los caracteres especiales que puedan ser conflictivos para nuestras cookies (Como por ejemplo si aparece ';' en el medio del nommre de una cookie.

//   var hoy = new Date();
//   hoy.setMonth(hoy. getMonth() + 1);

//   document.cookie = nombre+'='+valor+';expires='+hoy.toUTCString()+';';
// }

function borrarCookie(nombre) {
  var hoy = new Date();
  hoy.setMonth(hoy. getMonth() - 1);

  document.cookie = nombre+"=x;expires="+hoy.toUTCString()+";";
}

// function getCookie(nombre){
//   var cookies = document.cookie;

//   var cookieArr = cookies.split("; ");
//   console.log(cookieArr);

//   for(var i=0; i<cookieArr.length; i++){
//     var parArr = cookieArr[i].split('=');
//     cookieArr[i] = parArr;

//     if(parArr[0] === nombre){
//        unescape(parArr[1]);
//     }
// ;  }

//   return undefined;
// }

// console.log(getCookie('correo'));

// // var demo = "123;123*123'123/ 123";
// // console.log(escape(demo));
// // console.log(unescape(escape(demo)));

// // borrarCookie('apellido');

// // borrarCookie('nombre');
// // borrarCookie('correo');
// // borrarCookie('direccion');

// crearCookie('nombre', 'Leandro');
// crearCookie('correo', 'leandrito@gmail.com');
// crearCookie('direccion', 'Avenida Siempre Viva 123, Springfield');

// var cookies = document.cookie;

// console.log(cookies);

// getCookie('correo');


// // funciones bind, call, aply
// var carro  = {
//   color: "Blanco",
//   marca: "Mazda",
//   imprimir:function(){
//     var salida = this.marca + '-' + this.color;
//     return salida;
//   }
// }

// console.log(carro.imprimir());

// var logCarro = function(arg1,arg2){
//   console.log("Carro: ", this.imprimir());
//   console.log("Argumentos:", arg1, arg2);
//   console.log("========================");
// }

// var carro2 = { 
//   color: 'Rojo',
//   marca: 'Toyota'
// }

// // logCarro(); //no existe la funcion porque la busca en el global del programa. en windows, y esta dentro del objeto carro.

// var logModeloCarro = logCarro.bind(carro);

// logModeloCarro('asd','fgh');

// logModeloCarro.call(carro, 'asd', 'fgh');
// logModeloCarro.apply(carro,['asd','fgh']);


// // funciones prestadas
// console.log(carro.imprimir.call(carro2));

// // .46 JSON
// var objetoJS = { 
//   nombre:'Fernando',
//   edad: 30,
//   imprimir: function(){
//     console.log(this.nombre, this.edad);
//   }
// };

// console.log('Objeto literal:', objetoJS);

// var jsonString = JSON.stringify(objetoJS);

// console.log('Objeto Stringifeado:', jsonString);

// var objetoDesdeJson = JSON.parse(jsonString);

// console.log('Objeto parseado desde json:', objetoDesdeJson);

// //obs: al pasar a json se píerde la funcion imprirmir. Pero si la metes en el prototipo del objeto, se mantiene en json


// // .48 for in

// var Persona = function(){
//   this.nombre = 'Leandro';
//   this.apellido = 'Mendez';
//   this.edad = 18;
// };

// var juan = new Persona();

// Persona.prototype.direccion = 'San Jose';


// for(prop in juan){
//   if(!juan.hasOwnProperty(prop))
//     continue;

//   console.log(prop, ":", juan[prop]); 
// }


// for(num in [1,2,3,4,5,6,7,8,9,10]){
//   console.log(num); //num contiene los indices del array
// }

// // si queres referenciar a cada uno de los objetos de un array tenes que usar 

// [1,2,3,4,true,6,7,8,false,'nombre'].forEach(function(val){
//   console.log(val);
// })


// // .49 rotulando los ciclos

// for_principal:
// for (var i = 1; i <= 5; i++){
//   console.log('i', i);

//   for_secundario:
//   for(var j = 1; j <= 5; j++){
//     console.log('j',j)

//     for(var x = 1; x <= 5; x++){
//       console.log('x',x);

//       break for_principal;
//     }
//   }
// }


// .50 Funciones de tiempo

// setTimeout(function(){
//   console.log("paso un segundo");
// }, 1000);

// var segundos = 0;
// var intervalo = setInterval(function(){
//   segundos ++;

//   console.log('seg:', segundos);


//   if(segundos === 3){ // variable de corte
//     clearInterval(intervalo)
//   }
// }, 1000);

// var nombre = 'Pedro';
// var edad = 60;

// var juan = { 
//   nombre:'Juan',
//   edad: 30,
//   imprimir: function(){
//     var self = this;
//     setTimeout(function(){
//       console.log(self);
//       console.log(self.nombre, self.edad);
//     },1000);
//   }
// };

// juan.imprimir();


// // .51 eventos 101

// function evento(arg){
//   console.log('Me disparé');
//   console.log(arg);
// }

// var objeto = document.getElementById('objDemo');

// console.log(objeto);

// objeto.addEventListener('keypress', evento);

// objeto.click();


// // .52 desactivando click derecho en la pagina

// document.onmousedown = function(arg) {
//   if(arg.button === 2){
//     alert('Click bloqueado');
//     return;  
//   }  

//   console.log('No hay problema');
// }

// document.onmouseup = function() {
//   var texto = window.getSelection().toString();

//   console.log(texto);
// }


// // 53. on sumit - formularios

// function validar() {
//   var nombre = document.getElementById('txtNombre');
//   var nombre = document.getElementById('txtApellido');

//   if(nombre.lenght < 1){
//     return false;
//   }

//   if(apellido.lenght < 1){
//     return false;
//   }

//   return true;
// }

// // buscamos los parametros que pasamos por formulario
// console.log(window.location.search);

// //los podemos splitear por el caracter de corte, y los tendriamos en un array
// console.log(window.location.search.split('&'));


// function getParamURL(name) {
//   return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null
// }


// console.log(getParamURL('txtNombre'));



// // .54 cajas de dialogo

// alert('Hola Mundo');

// var acepto = confirm('Esta seguro que deasea borrar algo?');
// console.log(acepto);

// var nombre = prompt('Ingrese su nombre','nombre');
// console.log(nombre);



// function titleCase(str) {
//   return str.toLowerCase().split(' ').map(function(x) {
//     return x[0].toUpperCase() + x.slice(1);
//   }).join(' ');
// }


// console.log(titleCase("I'm a little tea pot"));

this.dataDay = [
  ["x", "2013-01-01", "2013-01-02", "2013-01-03", "2013-01-04", "2013-01-05", "2013-01-06", "2013-01-07",
        "2013-01-08", "2013-01-09", "2013-01-10", "2013-01-11", "2013-01-12", "2013-01-13", "2013-01-14",
        "2013-01-15", "2013-01-16", "2013-01-17", "2013-01-18", "2013-01-19", "2013-01-20", "2013-01-21",
        "2013-01-22", "2013-01-23", "2013-01-24", "2013-01-25", "2013-01-26", "2013-01-27", "2013-01-28",
        "2013-01-29", "2013-01-30", "2013-01-31",
        "2013-02-01", "2013-02-02", "2013-02-03", "2013-02-04", "2013-02-05", "2013-02-06", "2013-02-07",
        "2013-01-08", "2013-02-09", "2013-02-10", "2013-02-11", "2013-02-12", "2013-02-13", "2013-02-14",
        "2013-01-15", "2013-02-16", "2013-02-17", "2013-02-18", "2013-02-19", "2013-02-20", "2013-02-21",
        "2013-01-22", "2013-02-23", "2013-02-24", "2013-02-25", "2013-02-26", "2013-02-27", "2013-02-28"
  ],
  ["0", 3, 5, 20, 67, 45, 32, 67, 2, 67, 59, 11, 10, 34, 84, 50, 35, 54, 16, 22, 35, 2, 1, 25, 40, 5, 11, 67, 15, 19, 98, 100,
        2, 67, 59, 11, 10, 34, 1, 25, 40, 5, 11, 67, 15, 50, 35, 54, 16, 22, 35, 2, 3, 5, 20, 67, 45, 32, 67, 1
  ],
  ["1", 2, 67, 59, 11, 10, 34, 1, 25, 40, 5, 11, 67, 15, 50, 35, 54, 16, 22, 35, 2, 3, 5, 20, 67, 45, 32, 67, 1, 10, 43, 100,
        50, 35, 54, 16, 22, 35, 3, 5, 20, 67, 45, 32, 67, 1, 25, 40, 5, 11, 67, 15, 2, 67, 59, 11, 10, 34, 84, 23
  ],
  ["2", 50, 35, 54, 16, 22, 35, 3, 5, 20, 67, 45, 32, 67, 1, 25, 40, 5, 11, 67, 15, 2, 67, 59, 11, 10, 34, 84, 23, 90, 11, 100,
        1, 25, 40, 5, 11, 67, 50, 35, 54, 16, 22, 35, 2, 2, 67, 59, 11, 10, 34, 84, 3, 5, 20, 67, 45, 32, 67, 123, 5
  ],
  ["3", 1, 25, 40, 5, 11, 67, 50, 35, 54, 16, 22, 35, 2, 2, 67, 59, 11, 10, 34, 84, 3, 5, 20, 67, 45, 32, 67, 123, 5, 7, 10, 100,
        3, 5, 20, 67, 45, 32, 67, 2, 67, 59, 11, 10, 34, 84, 50, 35, 54, 16, 22, 35, 2, 1, 25, 40, 5, 11, 67, 15
  ]
];

this.dataWeek = daysToWeeks(this.dataDay);
console.log('dataWeek', this.dataWeek);

function daysToWeeks(dataDay) {
  function parteSemana(arr) {
    let i = 1;        
    let arrFin = [];
    const reducer = (accumulator, currentValue) => accumulator + currentValue;

    if(arr[0] === "x") {
      while(i < arr.length) {
        arrFin.push(arr.slice(i,i+1)[0]);       
        i += 7;        
      }      
      i -= 7;
      return arrFin;
    }    
    while(i < arr.length) {      
      arrFin.push(arr.slice(i, i+7).reduce(reducer));
      i += 7;
    }
    return arrFin;    
  }

  return dataDay.map(parteSemana);
}



// i = 1;
// while(i < this.dataDay[0].length){
//   console.log(this.dataDay[0].slice(i,i+1));
//   i += 7;
// }

