<?php
require_once('php-activerecord/ActiveRecord.php');

ActiveRecord\Config::initialize(function($cfg) // <----------- lambda functions -> buscar para entender este pasaje de funcion por parametro.
{                                              //               en php, closures.
  $cfg->set_model_directory('models');
  $cfg->set_connections(array(
     'development' => 'mysql://root:extendeal@localhost/practicaTrabajo?charset=utf8'));
});

// Si no tenes ganas de usar funciones anidadas
// $cfg = ActiveRecord\Config::instance();
// $cfg->set_model_directory('/path/to/your/model_directory');
// $cfg->set_connections(array('development' =>
//    'mysql://username:password@localhost/database_name'));


//creamos la clase User en el archivo User.php que extiende de la clase Activerecord\Model

# create Tito
$user = User::create(array('name' => 'Tito', 'state' => 'VA'));

# read Tito
$user = User::find_by_name('Tito');

# update Tito
$user->name = 'Tito Jr';
$user->save();

 # delete Tito
$user->delete();
