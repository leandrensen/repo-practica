<?php

namespace app\controllers;

use app\models\News;

class HelloController extends \lithium\action\Controller {
  public function index(){
    echo "Hello World!";
  }  

  public function goodbye($name) {
    return array (
      'name' => $name,
    );
  }

  public function news($id) {
    $news = News::first(array(
      'conditions' => array('id' => $id)
    ));

    return array(
          'news' => $news->data()  
        );
  }
}

?>
