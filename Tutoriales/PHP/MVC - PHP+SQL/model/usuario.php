<?php
class Usuario extends EntidadBase{
    private $id;
    private $nombre;
    private $apellido;
    private $email;
    private $nick;
    private $password;
     
    public function __construct() {
        $table="usuarios";
        parent::__construct($table);
    }
     
    public function getId() {
        return $this->id;
    }
 
    public function setId($id) {
        $this->id = $id;
    }
     
    public function getNombre() {
        return $this->nombre;
    }
 
    public function setNombre($nombre) {
        $this->nombre = $nombre;
    }
 
    public function getApellido() {
        return $this->apellido;
    }
 
    public function setApellido($apellido) {
        $this->apellido = $apellido;
    }
 
    public function getEmail() {
        return $this->email;
    }
 
    public function setEmail($email) {
        $this->email = $email;
    }

    public function getNick() {
        return $this->nick;
    }
    
    public function setNick($nick) {
        $this->nick = $nick;
    }
 
    public function getPassword() {
        return $this->password;
    }
 
    public function setPassword($password) {
        $this->password = $password;
    }
 
    public function save(){
        $query="INSERT INTO usuarios(nombre,apellido,email,nickname,password)
                VALUES('".$this->nombre."','".$this->apellido."','".$this->email."','".$this->nick."','".$this->password."');";
        //esto es hardcodeo para testear.
        // $query="INSERT INTO usuarios(id,nombre,apellido,email,nickname,password)
        //         VALUES(4,'Gonza','Gonza','Gonza@gmail.com','Gonzo','123456');";
        $save=$this->db()->query($query);
        
        if (!$save) {
           printf("Errormessage: %s\n", $this->db()->error);
        }

        //$this->db()->error;
        return $save;
    }
 
}
?>
