<?php 
  /* Ring Chart */

  /* pChart library inclusions */
  include("./class/pData.class.php");
  include("./class/pDraw.class.php");
  include("./class/pPie.class.php");
  include("./class/pImage.class.php");

  /* Create and populate the pData object */
  $MyData = new pData();   
  $MyData->addPoints(array(10,10,10,10,10,50), "ScoreA");  
  $MyData->setSerieDescription("ScoreA","Application A");

  /* Define the absissa serie */
  $MyData->addPoints(array("Bebidas","Almacen","Limpieza","Libreria","Congelados","Otros"), "Labels");
  $MyData->setAbscissa("Labels");

  /* Create the pChart object */
  $myPicture = new pImage(290,270,$MyData);

  /* Draw a solid background */
  // $Settings = array("R"=>170, "G"=>183, "B"=>87, "Dash"=>1, "DashR"=>190, "DashG"=>203, "DashB"=>107);
  // $myPicture->drawFilledRectangle(0,0,300,300,$Settings);

  /* Overlay with a gradient */
  // $Settings = array("StartR"=>219, "StartG"=>231, "StartB"=>139, "EndR"=>1, "EndG"=>138, "EndB"=>68, "Alpha"=>50);
  // $myPicture->drawGradientArea(0,0,300,260,DIRECTION_VERTICAL,$Settings);
  // $myPicture->drawGradientArea(0,0,300,20,DIRECTION_VERTICAL,array("StartR"=>0,"StartG"=>0,"StartB"=>0,"EndR"=>50,"EndG"=>50,"EndB"=>50,"Alpha"=>100));

  /* Add a border to the picture */
  $myPicture->drawRectangle(0,0,350,310,array("R"=>0,"G"=>0,"B"=>0));

  /* Write the picture title */ 
  $myPicture->setFontProperties(array("FontName"=>"fonts/SourceSansPro-Regular.ttf","FontSize"=>6));
  $myPicture->drawText(10,13,"pPie - Draw 2D ring charts",array("R"=>255,"G"=>255,"B"=>255));

  /* Set the default font properties */ 
  $myPicture->setFontProperties(array("FontName"=>"fonts/SourceSansPro-Regular.ttf","FontSize"=>10,"R"=>80,"G"=>80,"B"=>80));

  /* Enable shadow computing */ 
  // $myPicture->setShadow(TRUE,array("X"=>2,"Y"=>2,"R"=>0,"G"=>0,"B"=>0,"Alpha"=>50));

  /* Create the pPie object */ 
  $PieChart = new pPie($myPicture,$MyData);

  /* Define the slice color */

  $PieChart->setSliceColor(0,array("R"=>255,"G"=>213,"B"=>79));
  $PieChart->setSliceColor(1,array("R"=>255,"G"=>183,"B"=>77));
  $PieChart->setSliceColor(2,array("R"=>229,"G"=>115,"B"=>115));
  $PieChart->setSliceColor(3,array("R"=>156,"G"=>204,"B"=>101));
  $PieChart->setSliceColor(4,array("R"=>149,"G"=>117,"B"=>205));  
  $PieChart->setSliceColor(5,array("R"=>68,"G"=>138,"B"=>255));

  /* Draw an AA pie chart */ 
  $PieChart->draw2DRing(175,155,array("InnerRadius"=>800, "OuterRadius"=>150, "WriteValues"=>TRUE,
    "ValueR"=>255,"ValueG"=>255,"ValueB"=>255,"Border"=>FALSE, "ValuePosition"=>PIE_VALUE_INSIDE));

  /* Write the legend box */ 
  $myPicture->setShadow(FALSE);
  $PieChart->drawPieLegend(5,5,array("Alpha"=>20));

  /* Render the picture (choose the best way) */
  $myPicture->autoOutput("pictures/example.draw2DRingValue.png");
?>

<?php    
 /* CAT:Line chart */ 

 /* pChart library inclusions */ 
 include("./class/pData.class.php"); 
 include("./class/pDraw.class.php"); 
 include("./class/pImage.class.php"); 

 /* Create and populate the pData object */ 
 $myData = new pData();
 $myData->addPoints(array(21,27,32,42,4,10,30,35),"Serie1");
 $myData->setSerieDescription("Serie1","Serie 1");
 $myData->setSerieOnAxis("Serie1",0);

 $myData->addPoints(array(22,46,21,50,21,32,24,49),"Serie2");
 $myData->setSerieDescription("Serie2","Serie 2");
 $myData->setSerieOnAxis("Serie2",0);

 $myData->addPoints(array(45,33,39,35,22,25,50,6),"Serie3");
 $myData->setSerieDescription("Serie3","Serie 3");
 $myData->setSerieOnAxis("Serie3",0);

 $myData->addPoints(array("01/22","01/26","01/30","02/03","02/07","02/11","02/15","02/19"),"Absissa");
 $myData->setAbscissa("Absissa");

 $myData->setAxisPosition(0,AXIS_POSITION_LEFT);
 $myData->setAxisName(0,"1st axis");
 $myData->setAxisUnit(0,"");

 /* set palette of colors in lines */
 $serieSettings = array("R"=>68,"G"=>138,"B"=>255);
 $myData->setPalette("Serie1",$serieSettings);
 $serieSettings = array("R"=>255,"G"=>183,"B"=>77);
 $myData->setPalette("Serie2",$serieSettings);
 $serieSettings = array("R"=>229,"G"=>115,"B"=>115);
 $myData->setPalette("Serie3",$serieSettings);

 /* Create the pChart object */
 $myPicture = new pImage(700,230,$myData);
 $Settings = array("R"=>255, "G"=>255, "B"=>255);

 /* Add a border to the picture */ 
 $myPicture->drawFilledRectangle(0,0,700,230,$Settings);

 /* Define the chart area */ 
 $myPicture->setGraphArea(50,50,675,190);

 /* Set the default font */ 
 $myPicture->setFontProperties(array("R"=>0,"G"=>0,"B"=>0,"FontName"=>"fonts/SourceSansPro-Regular.ttf","FontSize"=>6));

 /* Draw the scale */ 
 $Settings = array("Pos"=>SCALE_POS_LEFTRIGHT
 , "Mode"=>SCALE_MODE_FLOATING
 , "LabelingMethod"=>LABELING_ALL
 , "GridR"=>255, "GridG"=>255, "GridB"=>255, "GridAlpha"=>50, "TickR"=>0, "TickG"=>0, "TickB"=>0, "TickAlpha"=>50, "LabelRotation"=>0, "CycleBackground"=>1, "DrawXLines"=>1, "DrawYLines"=>ALL);
 $myPicture->drawScale($Settings);

 /* Draw the line chart */
 $Config = "";
 $myPicture->drawLineChart($Config);

 /* Write the chart legend */ 
 $Config = array("FontR"=>0, "FontG"=>0, "FontB"=>0, "FontName"=>"fonts/SourceSansPro-Regular.ttf", "FontSize"=>6, "Margin"=>6, "Alpha"=>30, "BoxSize"=>5, "Style"=>LEGEND_NOBORDER
 , "Mode"=>LEGEND_HORIZONTAL
 );
 $myPicture->drawLegend(560,16,$Config);

 $myPicture->stroke();

?>
