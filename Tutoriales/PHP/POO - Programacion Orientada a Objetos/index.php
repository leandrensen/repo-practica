<?php
  trait Persona2 {
    public $nombre;

    public function mostrarNombre(){
      echo $this->nombre;
    }

    abstract function asignarNombre($nombre);
  }

  trait Trabajador {
    protected function mensaje() {
      echo "Mi nombre es: ";
    }
  }

  class Persona {
    use Persona2, Trabajador;

    public function asignarNombre($nombre) {
      $this->nombre = $nombre . self::mensaje();
    }
  }

  $persona = new Persona();
  $persona->asignarNombre("Carlos");
  $persona->mostrarNombre();  
?>

<?php
  // abstract class Molde {
  //   abstract public function ingresarNombre($nombre);
  //   abstract public function obtenerNombre();

  //   public static function mensaje($mensaje) {
  //     print $mensaje;
  //   }
  // }

  // class Persona extends Molde {
    
  //   private $mensaje = "Hola gente de Extendeal";

  //   public function mostrar() {
  //     print $this->mensaje;
  //   }

  //   public function ingresarNombre($nombre, $username = " ext") {
  //     $this->nombre = $nombre . $username;
  //   }

  //   public function obtenerNombre() {
  //       print $this->nombre;
  //   }
  // }

  // $obj = new Persona();
  // $obj->ingresarNombre("Carlos", " sarasa");
  // $obj->obtenerNombre();
?>

<?php
  // interface Auto {
  //   public function encender();
  //   public function apagar();
  // }

  // interface gasolina extends Auto {
  //   public function vaciarTanque();
  //   public function llenarTanque($cant);
  // }

  // class Deportivo implements gasolina {
    
  //   private $estado = false;
  //   private $tanque = 0;

  //   public function estado() {
  //     if($this->estado) {
  //       print "El auto esta encendido y tiene " . $this->tanque . " de litros en el tanque<br>";
  //     } else {
  //       print "El auto esta apagado<br>";
  //     }
  //   }

  //   public function encender() {
  //     if($this->estado){
  //       print "No puedes encender el auto dos veces<br>";
  //     }
  //     else{
  //       if($this->tanque <= 0) {
  //         print "Usted no puede encender el auto porque el tanque esta vacio<br>";
  //       } else {
  //         print "Auto encendido<br>";
  //         $this->estado = true;   
  //       }       
  //     }
  //   }

  //   public function apagar() {
  //     if($this->estado){
  //       print "Auto apagado<br>";
  //       $this->estado = false;
  //     }
  //     else{        
  //       print "El Auto ya esta apagado<br>";
  //       // $this->estado = true;
  //     }
  //   }

  //   public function vaciarTanque() {
  //     $this->tanque = 0;
  //   }

  //   public function llenarTanque($cant) {
  //     $this->tanque = $cant;
  //   }

  //   public function usar($km) {
  //     if($this->estado){ 
  //       $reducir = $km / 3;
  //       $this->tanque = $this->tanque - $reducir;
  //       if($this->tanque <= 0) {
  //         $this->estado = false;
  //       }
  //     } else {
  //       print "El auto esta apagado y no se puede usar<br>";
  //     }
  //   }
  // }

  // $obj = new Deportivo();
  // $obj->llenarTanque(100);
  // $obj->encender();
  // $obj->usar(350);
  // $obj->estado();
?>

<?php
  
  // class Facebook {

  //   //atributos
  //   public $nombre;
  //   public $edad;
  //   private $pass; //contraseña

  //   //metodos
  //   public function __construct($nombre, $edad, $pass){
  //       $this->nombre = $nombre;
  //       $this->edad = $edad;
  //       $this->pass = $pass;
  //   }

  //   public function verInformacion(){
  //     echo "Nombre: " . $this->nombre . "<br>";
  //     echo "Edad: " . $this->edad . "<br>";
  //     $this->cambiarPass("54321");
  //     echo "Password: " . $this->pass . "";
  //   }

  //   private function cambiarPass($pass) {
  //     $this->pass = $pass;
  //   }
  // }

  // $facebook = new Facebook("Leandro Mendez", 29, "1234");
  // // echo $facebook->pass;
  // // $facebook->cambiarPass("4321");
  // $facebook->verInformacion();
  
?>

<?php
  
  // class Loteria {
    
  //   //atributos
  //   public $numero;
  //   public $intentos;
  //   public $resultado = false;

  //   //metodos
  //   public function __construct($numero, $intentos) {
  //     $this->numero = $numero;
  //     $this->intentos = $intentos;
  //   }

  //   public function sortear() {
  //     $minimo = $this->numero / 2;
  //     $maximo = $this->numero * 2;     
  //     for($i = 0; $i < $this->intentos; $i++) {
  //       $int = rand($minimo, $maximo);
  //       self::intentos($int);
  //     }
  //   }

  //   public function intentos($int) {
  //     if($int == $this->numero){
  //       echo "<b>" . $int . " == " . $this->numero . "</b></br>";
  //       $this->resultado = true;
  //     } else {
  //       echo $int . " != " . $this->numero . "<br>";
  //     }
  //   }

  //   public function __destruct() {
  //     if($this->resultado) {
  //       echo "Felicidades, has ganado en  - $this->intentos " . "intentos.";
  //     } else {
  //       echo "Que lastima, has perdido en  - $this->intentos " . "intentos.";
  //     }
  //   }

  // }

  // $loteria = new Loteria(10,10);
  // $loteria->sortear();

?>

<?php

  // class Persona{

  //   //atributos
  //   public $nombre = array();
  //   public $apellido = array();

  //   //metodos
  //   public function guardar($nombre, $apellido) {
  //     $this->nombre[] = $nombre;
  //     $this->apellido[] = $apellido;
  //   }

  //   public function mostrar() {
  //     for($i = 0; $i < count($this->nombre); $i++){
  //       $this->formato($this->nombre[$i], $this->apellido[$i]);
  //     }
  //   }

  //   public function formato($nombre, $apellido) {
  //     echo "Nombre: " . $nombre . " | Apellido: " . $apellido . "<br>";
  //   }
  // }

  // $persona = new Persona();
  // $persona->guardar("Leandro", "Mendez");
  // $persona->guardar("Luciano", "Peluso");
  // $persona->mostrar();

?>

<?php

  // class Persona {
  //   //Atributos
  //   public $nombre = "Pedro";

  //   //Metodos
  //   public function hablar($mensaje) {
  //     echo $mensaje;
  //   }
  // }

  // $persona = new Persona();
  // //echo $persona->nombre;
  // $persona->hablar("Saludos desde Extendeal");
?>
