var insertContact = function(data) {
	if(data.isFavorite) {
		var ul = document.getElementsByClassName('contacts-list__favorites')[0];
	}	else {
		var ul = document.getElementsByClassName('contacts-list__other')[0];
	}  
  var str = '<li>' + 
      '<a onclick=' + 'renderContactPage('+ data.id +')' + '>' +
        '<img' +
            ' class="list__img-small-portrait"' +
            ' id="' + data.id.toString() + '"' +
            ' src="' + data.smallImageURL + '"' +
            ' onerror=defaultProfilePic("small",' + data.id.toString() + ')' +
            ' alt="Small Profile Pic"' +            
        '>' +
        '<span class="contacts-list__wrap">' +
          '<p class="list__name">';
  				if(data.isFavorite) str = str + '<img class="list__img-favorite-true" src="assets/favorite_star_true/favorite_true.png">';  				
          str = str + data.name +
          '</p>' +
          '<p class="list__company">' + (data.companyName == null ? '' : data.companyName) + '</p>' +
        '</span>' +
      '</a>' +
    '</li>';

  ul.insertAdjacentHTML('beforeend', str);    
}

var insertData = function(data) {
  data.forEach(function(element) {    
    insertContact(element);      
  });    
}

var defaultProfilePic = function(type, imageId) {
  if(type == 'small') {
    document.getElementById(imageId).src='assets/user_small/user_icon_small.png';  
  } else {
    document.getElementById(imageId).src='assets/user_large/user_icon_large.png';  
  }    
}  

var getContactById = function(JSONData, id) {
  var data;
  JSONData.forEach(function(elem){
    if(elem.id == id) data = elem;
  });
  return data;
}

var formatBirthDate = function(dateToFormat) {
  const monthNames = ["January", "February", "March", "April", "May", "June",
                     "July", "August", "September", "October", "November", "December"];
  const date = new Date(dateToFormat);

  const birthDate = {
    day: date.getDate().toString(),
    month: monthNames[date.getMonth()],
    year: date.getFullYear().toString()
  };                        

  return birthDate;
}  

var renderContactPage = function(id) {
  var win = window.open('contact.htm','_self',false);    
  win.data = getContactById(JSONData, id);    
  birthDate = formatBirthDate(data.birthdate);

  win.document.write(
    '<html><head><title>Contact</title><link rel="stylesheet" type="text/css" href="./styles/css/contact.css">' +      
    '<link rel="stylesheet" href="./font-awesome-4.7.0/css/font-awesome.min.css">' +       
    '</head><body>'    
  );
  win.document.write(
    '<div class="contacts">' +
      ' <div class="contacts__header">' +
        ' <span class="contacts__header__wrap">' +
          ' <a href="./index.html">' +
            ' <i class="fa fa-angle-left"></i><p class="contacts__header__title">Contacts</p>'); 
  if(win.data.isFavorite) {
    win.document.write(
              ' <img class="contacts__header__img-favorite-true" src="./assets/favorite_star_true/favorite_true.png">'
    );
  }              
  win.document.write(          
          ' </a>' +
        ' </span>' +
      ' </div>' +
      ' <div class="contacts__picture">' +
        ' <img' +
          ' class="contacts__picture__img-big-portrait"' +
          ' id="' + window.data.id.toString() + '"' +  
          ' src="' + window.data.largeImageURL + '"' +
          ' onerror=defaultProfilePic("large",' + window.data.id.toString() + ')' +
          ' alt="Big Profile Pic"' +
         ' >' +
        ' <p class="contacts__picture__full-name">' + window.data.name + '</p>' +
        ' <p class="contacts__picture__company-name">' + (window.data.companyName == null ? '' : window.data.companyName) + '</p>' +
      ' </div>' +
      ' <ul class="contacts__data">'
	);
  if(window.data.phone.home != null && window.data.phone.home != '') {
    win.document.write(
      		' <li class="contacts__data__phone">' +
          ' <p class="contacts__data__title">Phone:</p>' +
          ' <span class="contacts__data__wrap">' +
            ' <p class="contacts__data__phone__phone">' +  window.data.phone.home + '</p>' +
            ' <p class="contacts__data__phone__where">Home</p>' +
          ' </span>' +
        ' </li>' 	
    );      	
  }
  if(window.data.phone.mobile != null && window.data.phone.mobile != '') {
  	win.document.write(
      		' <li class="contacts__data__phone">' +
          ' <p class="contacts__data__title">Phone:</p>' +
          ' <span class="contacts__data__wrap">' +
            ' <p class="contacts__data__phone__phone">' + window.data.phone.mobile + '</p>' +
            ' <p class="contacts__data__phone__where">Mobile</p>' +
          ' </span>' +
        ' </li>' 	
  	); 
 	}
 	if(window.data.phone.work != null && window.data.phone.work != '') {
    win.document.write(
      		' <li class="contacts__data__phone">' +
          ' <p class="contacts__data__title">Phone:</p>' +
          ' <span class="contacts__data__wrap">' +
            ' <p class="contacts__data__phone__phone">' + window.data.phone.work + '</p>' +
            ' <p class="contacts__data__phone__where">Work</p>' +
          ' </span>' +
        ' </li>' 	
  	); 
 	}
 	win.document.write(       
        ' <li class="contacts__data__adress">' +
          ' <p class="contacts__data__title">Address:</p>' +
          ' <span class="contacts__data__wrap">' +
            ' <p class="contacts__data__adress__first">' + window.data.address.street + '</p>' +
            ' <p class="contacts__data__adress__second">' + window.data.address.city + ', ' + window.data.address.zipCode + ', ' + window.data.address.country + '</p>' +
          ' </span>' +
        ' </li>' +
        ' <li class="contacts__data__birthdate">' +
          ' <p class="contacts__data__title">Birthdate:</p>' +
          ' <p class="contacts__data__wrap">' + birthDate.month + ' ' + birthDate.day + ', ' + birthDate.year + '</p>' +
        ' </li>' +
        ' <li class="contacts__data__email">' +
          ' <p class="contacts__data__title">Email:</p>' +
          ' <p class="contacts__data__wrap">' + window.data.emailAddress + '</p>' +
        ' </li>' +
      ' </ul>' +
    ' </div>' +
  	'</body></html>'
  ); 
}

var orderDataByName = function(data) {
  return data.sort(function(a,b) {
    if(a.name < b.name) return -1;
    if(a.name > b.name) return 1;
    return 0;
  });
}  